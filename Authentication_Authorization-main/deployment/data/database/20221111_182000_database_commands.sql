DROP INDEX "idx_mfamodule$mfa_system$owner";
ALTER TABLE "mfamodule$mfa" RENAME TO "09abcdee62654b52a2b7e34095676b76";
ALTER TABLE "mfamodule$mfa_session" DROP CONSTRAINT "uniq_mfamodule$mfa_session_mfamodule$mfaid";
DROP INDEX "idx_mfamodule$mfa_session_system$session_mfamodule$mfa";
ALTER TABLE "mfamodule$mfa_session" RENAME TO "a82ddfc3c8534e3ea05c6874edfb9021";
DELETE FROM "mendixsystem$entity" 
 WHERE "id" = '8c9666c3-8da3-4ade-bd62-7d102aa738fc';
DELETE FROM "mendixsystem$entityidentifier" 
 WHERE "id" = '8c9666c3-8da3-4ade-bd62-7d102aa738fc';
DELETE FROM "mendixsystem$sequence" 
 WHERE "attribute_id" IN (SELECT "id"
 FROM "mendixsystem$attribute"
 WHERE "entity_id" = '8c9666c3-8da3-4ade-bd62-7d102aa738fc');
DELETE FROM "mendixsystem$remote_primary_key" 
 WHERE "entity_id" = '8c9666c3-8da3-4ade-bd62-7d102aa738fc';
DELETE FROM "mendixsystem$attribute" 
 WHERE "entity_id" = '8c9666c3-8da3-4ade-bd62-7d102aa738fc';
DELETE FROM "mendixsystem$index" 
 WHERE "table_id" = '8c9666c3-8da3-4ade-bd62-7d102aa738fc';
DELETE FROM "mendixsystem$index_column" 
 WHERE "index_id" IN ('07c0464a-1b23-3ef2-a9f6-7be03c4c3165');
DELETE FROM "mendixsystem$association" 
 WHERE "id" = 'c1848412-82a7-3769-b7a2-674595332442';
DELETE FROM "mendixsystem$association" 
 WHERE "id" = '9b4f86f3-3925-484d-85cf-bdbf7dbaac24';
DELETE FROM "mendixsystem$unique_constraint" 
 WHERE "name" = 'uniq_mfamodule$mfa_session_mfamodule$mfaid' AND "column_id" = '5aec0c95-ebbc-36c8-9f0c-48cbc3de85bb';
DROP TABLE "09abcdee62654b52a2b7e34095676b76";
DROP TABLE "a82ddfc3c8534e3ea05c6874edfb9021";
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221111 18:20:00';
