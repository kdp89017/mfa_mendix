DROP INDEX "idx_mfamodule$mfa_system$owner";
ALTER TABLE "mfamodule$mfa" RENAME TO "eb5bb1d3395547738057d748fe2b4d65";
ALTER TABLE "mfamodule$mfa_session" DROP CONSTRAINT "uniq_mfamodule$mfa_session_mfamodule$mfaid";
DROP INDEX "idx_mfamodule$mfa_session_system$session_mfamodule$mfa";
ALTER TABLE "mfamodule$mfa_session" RENAME TO "a49e98e63e16472c84c861130abf9e5b";
ALTER TABLE "googleauthenticator$google_credentials_user" DROP CONSTRAINT "uniq_googleauthenticator$google_credentials_user_googleauthenticator$google_credentialsid";
DROP INDEX "idx_googleauthenticator$google_credentials_user_system$user_googleauthenticator$google_credentials";
ALTER TABLE "googleauthenticator$google_credentials_user" RENAME TO "d8e5a328603d4bbdabb1cf0e61a7c4af";
DELETE FROM "mendixsystem$entity" 
 WHERE "id" = '8e7e44b5-973e-44e8-9b14-9d653995f619';
DELETE FROM "mendixsystem$entityidentifier" 
 WHERE "id" = '8e7e44b5-973e-44e8-9b14-9d653995f619';
DELETE FROM "mendixsystem$sequence" 
 WHERE "attribute_id" IN (SELECT "id"
 FROM "mendixsystem$attribute"
 WHERE "entity_id" = '8e7e44b5-973e-44e8-9b14-9d653995f619');
DELETE FROM "mendixsystem$remote_primary_key" 
 WHERE "entity_id" = '8e7e44b5-973e-44e8-9b14-9d653995f619';
DELETE FROM "mendixsystem$attribute" 
 WHERE "entity_id" = '8e7e44b5-973e-44e8-9b14-9d653995f619';
DELETE FROM "mendixsystem$index" 
 WHERE "table_id" = '8e7e44b5-973e-44e8-9b14-9d653995f619';
DELETE FROM "mendixsystem$index_column" 
 WHERE "index_id" IN ('c689cc94-1cad-3861-960f-68afd9e306bb');
DELETE FROM "mendixsystem$association" 
 WHERE "id" = 'c1d06186-c949-3df3-b374-31f1542a6a2b';
DELETE FROM "mendixsystem$association" 
 WHERE "id" = '97bcb888-f09e-479f-8889-338636f82fa5';
DELETE FROM "mendixsystem$unique_constraint" 
 WHERE "name" = 'uniq_mfamodule$mfa_session_mfamodule$mfaid' AND "column_id" = '581b5e98-92f2-3cf6-b260-c2bc5732d1d8';
DELETE FROM "mendixsystem$association" 
 WHERE "id" = '1edb73c6-8ee4-4e0b-baac-10598676b15d';
DELETE FROM "mendixsystem$unique_constraint" 
 WHERE "name" = 'uniq_googleauthenticator$google_credentials_user_googleauthenticator$google_credentialsid' AND "column_id" = '1b3a1c73-371e-3c0c-85a6-571a9ade6165';
CREATE TABLE "mfamodule$mfa" (
	"id" BIGINT NOT NULL,
	"code" VARCHAR_IGNORECASE(200) NULL,
	"showrequestcode" BOOLEAN NULL,
	"token" VARCHAR_IGNORECASE(2147483647) NULL,
	"username" VARCHAR_IGNORECASE(200) NULL,
	"message" VARCHAR_IGNORECASE(200) NULL,
	"messagetype" VARCHAR_IGNORECASE(5) NULL,
	"createddate" TIMESTAMP NULL,
	"system$owner" BIGINT NULL,
	PRIMARY KEY("id"));
CREATE INDEX "idx_mfamodule$mfa_system$owner" ON "mfamodule$mfa" ("system$owner" ASC,"id" ASC);
INSERT INTO "mendixsystem$entity" ("id", 
"entity_name", 
"table_name", 
"remote", 
"remote_primary_key")
 VALUES ('8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'MFAmodule.MFA', 
'mfamodule$mfa', 
false, 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('43904550-e2b4-4e2a-9282-4aeef583efea', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'Code', 
'code', 
30, 
200, 
'', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('3cf234b2-b93e-486c-9626-534a8b420aca', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'ShowRequestCode', 
'showrequestcode', 
10, 
0, 
'false', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('da87f40a-b3d5-4e42-a9dd-a37d75464c02', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'Token', 
'token', 
30, 
0, 
'', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('698f8732-4318-4cd8-be67-8fc7dc3b66d2', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'Username', 
'username', 
30, 
200, 
'', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('fde3881d-d119-439c-b3bf-e159750911a4', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'Message', 
'message', 
30, 
200, 
'', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('deda2937-fd17-4f80-b3c9-4b6377cfb527', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'MessageType', 
'messagetype', 
40, 
5, 
'', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('c4cfe3e8-073c-399e-9edf-9dcfa9b82fad', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'createdDate', 
'createddate', 
20, 
0, 
'', 
false);
INSERT INTO "mendixsystem$index" ("id", 
"table_id", 
"index_name")
 VALUES ('07c0464a-1b23-3ef2-a9f6-7be03c4c3165', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'idx_mfamodule$mfa_system$owner');
INSERT INTO "mendixsystem$index_column" ("index_id", 
"column_id", 
"sort_order", 
"ordinal")
 VALUES ('07c0464a-1b23-3ef2-a9f6-7be03c4c3165', 
'c1848412-82a7-3769-b7a2-674595332442', 
false, 
0);
INSERT INTO "mendixsystem$association" ("id", 
"association_name", 
"table_name", 
"parent_entity_id", 
"child_entity_id", 
"parent_column_name", 
"child_column_name")
 VALUES ('c1848412-82a7-3769-b7a2-674595332442', 
'System.owner', 
'system$owner', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'282e2e60-88a5-469d-84a5-ba8d9151644f', 
'id', 
'system$owner');
CREATE TABLE "mfamodule$mfa_session" (
	"mfamodule$mfaid" BIGINT NOT NULL,
	"system$sessionid" BIGINT NOT NULL,
	PRIMARY KEY("mfamodule$mfaid","system$sessionid"),
	CONSTRAINT "uniq_mfamodule$mfa_session_mfamodule$mfaid" UNIQUE ("mfamodule$mfaid"));
CREATE INDEX "idx_mfamodule$mfa_session_system$session_mfamodule$mfa" ON "mfamodule$mfa_session" ("system$sessionid" ASC,"mfamodule$mfaid" ASC);
INSERT INTO "mendixsystem$association" ("id", 
"association_name", 
"table_name", 
"parent_entity_id", 
"child_entity_id", 
"parent_column_name", 
"child_column_name", 
"index_name")
 VALUES ('9b4f86f3-3925-484d-85cf-bdbf7dbaac24', 
'MFAmodule.MFA_Session', 
'mfamodule$mfa_session', 
'8c9666c3-8da3-4ade-bd62-7d102aa738fc', 
'37f9fd49-5318-4c63-9a51-f761779b202f', 
'mfamodule$mfaid', 
'system$sessionid', 
'idx_mfamodule$mfa_session_system$session_mfamodule$mfa');
INSERT INTO "mendixsystem$unique_constraint" ("name", 
"table_id", 
"column_id")
 VALUES ('uniq_mfamodule$mfa_session_mfamodule$mfaid', 
'9b4f86f3-3925-484d-85cf-bdbf7dbaac24', 
'5aec0c95-ebbc-36c8-9f0c-48cbc3de85bb');
CREATE TABLE "googleauthenticator$google_credentials_user_2" (
	"googleauthenticator$google_credentialsid" BIGINT NOT NULL,
	"system$userid" BIGINT NOT NULL,
	PRIMARY KEY("googleauthenticator$google_credentialsid","system$userid"),
	CONSTRAINT "uniq_googleauthenticator$google_credentials_user_2_googleauthenticator$google_credentialsid" UNIQUE ("googleauthenticator$google_credentialsid"));
CREATE INDEX "idx_googleauthenticator$google_credentials_user_2_system$user_googleauthenticator$google_credentials" ON "googleauthenticator$google_credentials_user_2" ("system$userid" ASC,"googleauthenticator$google_credentialsid" ASC);
INSERT INTO "mendixsystem$association" ("id", 
"association_name", 
"table_name", 
"parent_entity_id", 
"child_entity_id", 
"parent_column_name", 
"child_column_name", 
"index_name")
 VALUES ('04619954-8604-4eea-926e-d9193dfc2fdc', 
'GoogleAuthenticator.Google_Credentials_User_2', 
'googleauthenticator$google_credentials_user_2', 
'97711df7-8416-498f-9346-6d5318207782', 
'282e2e60-88a5-469d-84a5-ba8d9151644f', 
'googleauthenticator$google_credentialsid', 
'system$userid', 
'idx_googleauthenticator$google_credentials_user_2_system$user_googleauthenticator$google_credentials');
INSERT INTO "mendixsystem$unique_constraint" ("name", 
"table_id", 
"column_id")
 VALUES ('uniq_googleauthenticator$google_credentials_user_2_googleauthenticator$google_credentialsid', 
'04619954-8604-4eea-926e-d9193dfc2fdc', 
'a80bf0f6-b416-3263-a2a3-bf06700903b2');
DROP TABLE "eb5bb1d3395547738057d748fe2b4d65";
DROP TABLE "a49e98e63e16472c84c861130abf9e5b";
DROP TABLE "d8e5a328603d4bbdabb1cf0e61a7c4af";
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221111 15:21:10';
