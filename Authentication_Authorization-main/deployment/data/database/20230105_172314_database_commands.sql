ALTER TABLE "myfirstmodule$barcode_value" RENAME TO "8c3f0df2e2d64948a3adc25ad54f9152";
DELETE FROM "mendixsystem$entity" 
 WHERE "id" = 'b78b0077-ba2a-425b-9c4d-f8fc73884466';
DELETE FROM "mendixsystem$entityidentifier" 
 WHERE "id" = 'b78b0077-ba2a-425b-9c4d-f8fc73884466';
DELETE FROM "mendixsystem$sequence" 
 WHERE "attribute_id" IN (SELECT "id"
 FROM "mendixsystem$attribute"
 WHERE "entity_id" = 'b78b0077-ba2a-425b-9c4d-f8fc73884466');
DELETE FROM "mendixsystem$remote_primary_key" 
 WHERE "entity_id" = 'b78b0077-ba2a-425b-9c4d-f8fc73884466';
DELETE FROM "mendixsystem$attribute" 
 WHERE "entity_id" = 'b78b0077-ba2a-425b-9c4d-f8fc73884466';
DROP TABLE "8c3f0df2e2d64948a3adc25ad54f9152";
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20230105 17:23:14';
