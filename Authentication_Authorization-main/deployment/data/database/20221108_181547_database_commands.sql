ALTER TABLE "barcode$testoutput" RENAME TO "ebc5b5f25e074bf2823dde616e9d992f";
DELETE FROM "mendixsystem$entity" 
 WHERE "id" = '1b9214ea-42bb-4bcc-9981-fbb87633802e';
DELETE FROM "mendixsystem$entityidentifier" 
 WHERE "id" = '1b9214ea-42bb-4bcc-9981-fbb87633802e';
DELETE FROM "mendixsystem$sequence" 
 WHERE "attribute_id" IN (SELECT "id"
 FROM "mendixsystem$attribute"
 WHERE "entity_id" = '1b9214ea-42bb-4bcc-9981-fbb87633802e');
DELETE FROM "mendixsystem$remote_primary_key" 
 WHERE "entity_id" = '1b9214ea-42bb-4bcc-9981-fbb87633802e';
DELETE FROM "mendixsystem$attribute" 
 WHERE "entity_id" = '1b9214ea-42bb-4bcc-9981-fbb87633802e';
CREATE TABLE "barcode$testoutput" (
	"id" BIGINT NOT NULL,
	PRIMARY KEY("id"));
INSERT INTO "mendixsystem$entity" ("id", 
"entity_name", 
"table_name", 
"superentity_id", 
"remote", 
"remote_primary_key")
 VALUES ('c8d6e4cc-676d-4b22-9702-2cc0de4713ea', 
'Barcode.TestOutput', 
'barcode$testoutput', 
'37827192-315d-4ab6-85b8-f626f866ea76', 
false, 
false);
DELETE FROM "system$image" 
 WHERE "id" IN (SELECT "id"
 FROM "ebc5b5f25e074bf2823dde616e9d992f");
DELETE FROM "system$thumbnail_image" 
 WHERE "system$imageid" IN (SELECT "id"
 FROM "ebc5b5f25e074bf2823dde616e9d992f");
DELETE FROM "system$filedocument" 
 WHERE "id" IN (SELECT "id"
 FROM "ebc5b5f25e074bf2823dde616e9d992f");
DROP TABLE "ebc5b5f25e074bf2823dde616e9d992f";
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221108 18:15:47';
