CREATE TABLE "myfirstmodule$secreteqrcode" (
	"id" BIGINT NOT NULL,
	"url" VARCHAR_IGNORECASE(200) NULL,
	PRIMARY KEY("id"));
INSERT INTO "mendixsystem$entity" ("id", 
"entity_name", 
"table_name", 
"superentity_id", 
"remote", 
"remote_primary_key")
 VALUES ('472248c6-776b-45de-8dcd-189a3d54f7ee', 
'MyFirstModule.SecreteQRCode', 
'myfirstmodule$secreteqrcode', 
'37827192-315d-4ab6-85b8-f626f866ea76', 
false, 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('a60bec3d-f157-40f4-a302-c251e65954d6', 
'472248c6-776b-45de-8dcd-189a3d54f7ee', 
'url', 
'url', 
30, 
200, 
'', 
false);
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221108 18:22:49';
