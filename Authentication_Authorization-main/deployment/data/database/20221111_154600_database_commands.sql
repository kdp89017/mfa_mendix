ALTER TABLE "googleauthenticator$google_credentials" ADD "username" VARCHAR_IGNORECASE(200) NULL;
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('7777a5fb-517e-462d-9a7b-f69d931c2265', 
'97711df7-8416-498f-9346-6d5318207782', 
'username', 
'username', 
30, 
200, 
'', 
false);
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221111 15:46:00';
