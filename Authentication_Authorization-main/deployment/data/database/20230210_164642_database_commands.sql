CREATE TABLE "myfirstmodule$item" (
	"id" BIGINT NOT NULL,
	"item_name" VARCHAR_IGNORECASE(200) NULL,
	"price" DECIMAL(28, 8) NULL,
	PRIMARY KEY("id"));
INSERT INTO "mendixsystem$entity" ("id", 
"entity_name", 
"table_name", 
"superentity_id", 
"remote", 
"remote_primary_key")
 VALUES ('380305fd-d7f0-41d3-ba55-3df4b352c6b3', 
'MyFirstModule.Item', 
'myfirstmodule$item', 
'37827192-315d-4ab6-85b8-f626f866ea76', 
false, 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('0e9538c5-d946-4273-b392-5c9b68e0e4d3', 
'380305fd-d7f0-41d3-ba55-3df4b352c6b3', 
'item_name', 
'item_name', 
30, 
200, 
'', 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('818f3ad3-c080-4b5e-91dc-b09b7a8af281', 
'380305fd-d7f0-41d3-ba55-3df4b352c6b3', 
'price', 
'price', 
5, 
0, 
'0', 
false);
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20230210 16:46:42';
