CREATE TABLE "googleauthenticator$google_credentials_user" (
	"googleauthenticator$google_credentialsid" BIGINT NOT NULL,
	"system$userid" BIGINT NOT NULL,
	PRIMARY KEY("googleauthenticator$google_credentialsid","system$userid"),
	CONSTRAINT "uniq_googleauthenticator$google_credentials_user_googleauthenticator$google_credentialsid" UNIQUE ("googleauthenticator$google_credentialsid"));
CREATE INDEX "idx_googleauthenticator$google_credentials_user_system$user_googleauthenticator$google_credentials" ON "googleauthenticator$google_credentials_user" ("system$userid" ASC,"googleauthenticator$google_credentialsid" ASC);
INSERT INTO "mendixsystem$association" ("id", 
"association_name", 
"table_name", 
"parent_entity_id", 
"child_entity_id", 
"parent_column_name", 
"child_column_name", 
"index_name")
 VALUES ('1edb73c6-8ee4-4e0b-baac-10598676b15d', 
'GoogleAuthenticator.Google_Credentials_User', 
'googleauthenticator$google_credentials_user', 
'97711df7-8416-498f-9346-6d5318207782', 
'282e2e60-88a5-469d-84a5-ba8d9151644f', 
'googleauthenticator$google_credentialsid', 
'system$userid', 
'idx_googleauthenticator$google_credentials_user_system$user_googleauthenticator$google_credentials');
INSERT INTO "mendixsystem$unique_constraint" ("name", 
"table_id", 
"column_id")
 VALUES ('uniq_googleauthenticator$google_credentials_user_googleauthenticator$google_credentialsid', 
'1edb73c6-8ee4-4e0b-baac-10598676b15d', 
'1b3a1c73-371e-3c0c-85a6-571a9ade6165');
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221109 21:39:42';
