CREATE TABLE "myfirstmodule$barcode_value" (
	"id" BIGINT NOT NULL,
	"barcode_value" VARCHAR_IGNORECASE(200) NULL,
	PRIMARY KEY("id"));
INSERT INTO "mendixsystem$entity" ("id", 
"entity_name", 
"table_name", 
"remote", 
"remote_primary_key")
 VALUES ('b78b0077-ba2a-425b-9c4d-f8fc73884466', 
'MyFirstModule.barcode_value', 
'myfirstmodule$barcode_value', 
false, 
false);
INSERT INTO "mendixsystem$attribute" ("id", 
"entity_id", 
"attribute_name", 
"column_name", 
"type", 
"length", 
"default_value", 
"is_auto_number")
 VALUES ('69aa7020-05da-4209-917f-3facac91e81f', 
'b78b0077-ba2a-425b-9c4d-f8fc73884466', 
'barcode_value', 
'barcode_value', 
30, 
200, 
'', 
false);
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221228 15:11:27';
