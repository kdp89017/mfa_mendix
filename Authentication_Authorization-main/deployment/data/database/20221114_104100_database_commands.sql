ALTER TABLE "administration$account" DROP COLUMN "hasmfaenabled";
ALTER TABLE "administration$account" DROP COLUMN "lastlogin2fa";
DELETE FROM "mendixsystem$attribute" 
 WHERE "id" = '1dad4b61-cbea-4bdd-8222-0fb6592b26ae';
DELETE FROM "mendixsystem$attribute" 
 WHERE "id" = '26c29cdb-d017-4fe1-ae9b-adde63c750df';
UPDATE "mendixsystem$version"
 SET "versionnumber" = '4.2', 
"lastsyncdate" = '20221114 10:41:00';
